<?php

class EnvLoader
{
	public function load($paths)
	{
		$dotenv = Dotenv\Dotenv::create($paths);
		$dotenv->load();
	}
}


