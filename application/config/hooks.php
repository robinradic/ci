<?php if ( ! defined('BASEPATH')) {
	exit('No direct script access allowed');
}
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook[ 'pre_system' ][]       = [
	'class'    => 'EnvLoader',
	'function' => 'load',
	'filename' => 'EnvLoader.php',
	'filepath' => 'hooks',
	'params'   => [ dirname(__DIR__, 2) ],
];
$hook[ 'display_override' ][] = [
	'class'    => 'Develbar',
	'function' => 'debug',
	'filename' => 'Develbar.php',
	'filepath' => 'third_party/DevelBar/hooks',
];

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */
